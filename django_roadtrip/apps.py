from django.apps import AppConfig


class RoadtripConfig(AppConfig):
    name = 'django_roadtrip'
    verbose_name = "Voyages"
