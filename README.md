# django-roadtrip

Django application to inform and follow your friends during road trips.

## Quick start

1. Add some settings
```
INSTALLED_APPS = [
    'django_leaflet_gpx',
    'django_roadtrip',
    'django.contrib.humanize',
]

MIDDLEWARE = [
    'django.middleware.locale.LocaleMiddleware',
]
`̀``

2. Include the django-roadtrip in your project urls.py
```
urlpatterns = [
    path('roadtrip/', include(('django_roadtrip.urls', 'roadtrip'))),
]
```

3. Migrate database
```
python manage.py migrate
```

4. Start the development server and visit http://127.0.0.1:8000/blog to check if everything is ok.

